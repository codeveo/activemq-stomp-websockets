ActiveMQ, STOMP and Websockets integration
==========================================
Demonstration of a simple integration of ActiveMQ, STOMP and websockets. The JS clients are connecting to ActiveMQ message broker via STOMP protocol over websockets.

There is one common topic called 'example.common' created at broker startup.
See application.properties for configuration.

Instructions:
-------------
	1) mvn clean install which will automatically start a test and run it
       for 2 minutes

	2) Open two browser tabs and in both goto localhost:8080. In the first
       one subscribe to /queue/example.q1 and in the second subscribe to  
       /queue/example.q2
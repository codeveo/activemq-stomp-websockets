package com.codeveo.examples.activemq.stomp.ws;

import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQTopic;
import org.apache.camel.ProducerTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

@ContextConfiguration(classes = {
                AppConfig.class
})
public class TestApp extends AbstractTestNGSpringContextTests {

    private static final Logger LOG = LoggerFactory.getLogger(TestApp.class.getName());

    private final AtomicInteger counter = new AtomicInteger(0);

    private final Random rand = new Random(System.currentTimeMillis());

    private final ScheduledExecutorService executors = Executors.newScheduledThreadPool(3);

    @Value("${destination.name.prefix}")
    private String destinationNamePrefix;

    @Autowired
    private BrokerService broker;

    @Autowired
    private ProducerTemplate producerTemplate;

    @Autowired
    private JMXDiscovery jmxDiscovery;

    @Test
    public void testWSHttpCall() throws Exception {
        executors.scheduleAtFixedRate(() -> {
            try {
                final List<ActiveMQDestination> destinations = jmxDiscovery.getDestinationsByPrefix(destinationNamePrefix);

                if (destinations.isEmpty()) {
                    LOG.info("No chat queue or topic registered");
                    return;
                }

                final ActiveMQDestination dest = destinations.get(Math.abs(rand.nextInt()) % destinations.size());

                final String destName = "activemq:" + ((dest instanceof ActiveMQTopic) ? "topic" : "queue") + ":" + dest
                                .getPhysicalName();

                final String msg = "Message #" + counter.getAndIncrement();

                LOG.info("Sending message '{}' to destination '{}'", msg, dest.getPhysicalName());

                producerTemplate.sendBody(destName, msg);
            } catch (final Exception e) {
                LOG.error("Error occured during test", e);
            }
        } , 0, 2, TimeUnit.SECONDS);

        Thread.sleep(120 * 1000);
    }

    @AfterClass
    public void tearDown() throws Exception {
        executors.shutdownNow();
        broker.stop();
    }
}

package com.codeveo.examples.activemq.stomp.ws;

import org.apache.camel.ProducerTemplate;
import org.apache.camel.spring.javaconfig.CamelConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RouteConfiguration extends CamelConfiguration {

    @Bean
    public ProducerTemplate producerTemplate() throws Exception {
        return camelContext().createProducerTemplate();
    }
}

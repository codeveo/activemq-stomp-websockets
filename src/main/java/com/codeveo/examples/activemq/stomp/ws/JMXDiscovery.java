package com.codeveo.examples.activemq.stomp.ws;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerInvocationHandler;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.apache.activemq.broker.jmx.BrokerViewMBean;
import org.apache.activemq.broker.jmx.QueueViewMBean;
import org.apache.activemq.broker.jmx.TopicViewMBean;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

@Service
@DependsOn("broker")
public class JMXDiscovery {

    private BrokerViewMBean mbean;

    private MBeanServerConnection connection;

    public JMXDiscovery() {
        try {
            final JMXServiceURL url = new JMXServiceURL("service:jmx:rmi:///jndi/rmi://localhost:1099/jmxrmi");
            final JMXConnector connector = JMXConnectorFactory.connect(url, null);
            connector.connect();
            connection = connector.getMBeanServerConnection();
        } catch (final Exception e) {
            throw new IllegalStateException("Error occured durring JMX discovery service initialization", e);
        }
    }

    public List<ActiveMQDestination> getDestinationsByPrefix(final String prefix) throws Exception {
        final ObjectName name = new ObjectName("org.apache.activemq:type=Broker,brokerName=localhost");
        mbean = MBeanServerInvocationHandler.newProxyInstance(connection, name, BrokerViewMBean.class, true);

        final List<ActiveMQDestination> destinations = Arrays.asList(mbean.getQueues()).stream().map(
                        obj -> new ActiveMQQueue(MBeanServerInvocationHandler.newProxyInstance(connection, obj,
                                        QueueViewMBean.class, true).getName())).collect(Collectors.toList());

        destinations.addAll(Arrays.asList(mbean.getTopics()).stream().map(obj -> new ActiveMQTopic(
                        MBeanServerInvocationHandler.newProxyInstance(connection, obj, TopicViewMBean.class, true)
                                        .getName())).collect(Collectors.toList()));

        return destinations.stream().filter(dest -> dest.getPhysicalName().startsWith(prefix) && !dest.getPhysicalName()
                        .contains("Advisory")).collect(Collectors.toList());
    }

}

package com.codeveo.examples.activemq.stomp.ws;

import java.io.IOException;

import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ActiveMQTopic;
import org.eclipse.jetty.server.Server;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan("com.codeveo.examples")
@PropertySource("classpath:application.properties")
public class AppConfig {

    @Value("${common.topic.name}")
    private String commonTopicName;

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean(initMethod = "start",
          destroyMethod = "stop")
    public Server microHttpServer(@Value("${http.server.port}") final int httpServerPort) throws IOException {
        final Server server = new Server(httpServerPort);
        server.setHandler(new ClientHandler());
        return server;
    }

    @Bean(name = "broker",
          initMethod = "start",
          destroyMethod = "stop")
    public BrokerService broker(@Value("${jms.broker.url}") final String jmsBrokerURL,
                                @Value("${ws.broker.url}") final String wsBrokerURL) throws Exception {
        final BrokerService broker = new BrokerService();
        broker.addConnector(wsBrokerURL);
        broker.addConnector(jmsBrokerURL);
        broker.setPersistent(false);

        broker.setDestinations(new ActiveMQDestination[] {
                        new ActiveMQTopic(commonTopicName)
        });

        return broker;
    }

}

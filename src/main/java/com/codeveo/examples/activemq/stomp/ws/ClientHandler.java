package com.codeveo.examples.activemq.stomp.ws;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class ClientHandler extends AbstractHandler {

    final String body;

    public ClientHandler() throws IOException {
        body = Resources.toString(Resources.getResource("index.html"), Charsets.UTF_8);
    }

    @Override
    public void handle(final String target, final Request baseRequest, final HttpServletRequest request,
                       final HttpServletResponse response) throws IOException, ServletException {
        response.setContentType("text/html; charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);

        if (body != null) {
            response.getWriter().println(body);
        }

        baseRequest.setHandled(true);
    }

}

package com.codeveo.examples.activemq.stomp.ws;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ClientCallRoute extends RouteBuilder {

    @Value("${jms.broker.url}")
    private String jmsBrokerURL;

    @Override
    public void configure() throws Exception {
        final ActiveMQComponent activemqComponent = (ActiveMQComponent) getContext().getComponent("activemq");
        activemqComponent.setBrokerURL(jmsBrokerURL);

        from("activemq:queue:from.client").routeId("from-client").log(LoggingLevel.INFO,
                        ">> Client sent message: '${body}' (HEADER: header: '${in.headers})'");
    }

}
